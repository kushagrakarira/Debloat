# Deblot
![IMG](https://img.shields.io/badge/licence-GPL3-blue?style=for-the-badge)
![IMG](https://img.shields.io/github/stars/kushagrakarira/Debloat?style=for-the-badge&logo=github)
![IMG](https://img.shields.io/tokei/lines/github/kushagrakarira/Debloat?style=for-the-badge&logo=github)
![IMG](https://img.shields.io/github/repo-size/kushagrakarira/Debloat?label=SIZE&logo=github&style=for-the-badge)

## How to use :

1. [Download](https://github.com/KushagraKarira/Debloat/archive/refs/heads/master.zip) the repo. or clone it in your machine
2. Find the script and Edit it as per your needs ( Comment out apps that you need )
3. Save and Execute...
4. Feedback !! - Seprate form has been created on issues page

## Caution
### Remember to backup your chats and photos
### In case of an accident go to [ApkMirror.com](https://www.apkmirror.com/) and help yourself

## Screenshots
<img src=HomeScreen.png width=40% height=40% align: left>

## Contributors
![GitHub Contributors Image](https://contrib.rocks/image?repo=kushagrakarira/Debloat)
